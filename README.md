## nc_proj

Geotagging a NetCDF file according with the CF-Conventions requires adding some specific metadata:
Horizontal coordinate variables (x,y; lat,lon), CRS (Coordinates Reference System) variable and the respective attributes.
The *nc_proj* python module provides an interface to create the NetCDF Projection variables and attributes from a given PROJ string and GeoTransform array.
It wrapps some of the GDAL library (netcdf driver) functions into python bindings, which makes the libgdal-dev a mandatory dependency.
Actually, the GDAL library netcdf driver already do this job. However, with *nc_proj* you can create NetCDF files with your favorite NetCDF/HDF5 interface (xarray, netCDF4, h5py, pytables,...) and write the Projection variables/attributes on your own.

Checkout the GDAL and PROJ reference documentation for details about how to build valid a PROJ string (define projection) and GeoTransform array (define spatial extent/boundaries and resolution).

* https://www.gdal.org/gdal_datamodel.html
* https://proj4.org/operations/projections/index.html

INSTALL:

with setup.py:

<code>python setup.py install<code/>

If the GDAL library isn't installed in the system default path:

<code>python setup.py build_ext -I/path/to/gdal/headers/include -L/path/to/gdal/library/lib install<code/>

with pip:

<code>pip install .<code/>

If the GDAL library isn't installed in the system default path:

<code>pip install --global-option=build_ext --global-option="-I/path/to/gdal/headers/include" --global-option="-L/path/to/gdal/library/lib" .<code/>

The SWIG files (nc_proj.py and nc_proj_wrap.cpp) were generated with the following command:

<code>swig -python -c++ -outcurrentdir nc_proj.i<code/>


Example:
```
>>> from nc_proj import nc_proj
>>> 
>>> proj_str = ("+proj=geos +lon_0=0 +h=35785831 "
>>>             "+x_0=0 +y_0=0 +a=6371229 b=6349867.475461252 "
>>>             "+units=m +sweep=y +no_defs")
>>>
>>> nc_proj.nc_grid_mapping_attrs(proj_str)
{'grid_mapping_name': b'geostationary',
 'long_name': b'CRS definition',
 'sweep_angle_axis': b'y',
 'false_easting': 0.0,
 'false_northing': 0.0,
 'inverse_flattening': 298.2572235630064,
 'longitude_of_prime_meridian': 0.0,
 'longitude_of_projection_origin': 0.0,
 'perspective_point_height': 35785831.0,
 'semi_major_axis': 6371229.0,
 'standard_parallel': 0.0}
>>>
>>> nc_proj.nc_xcoord_attrs(proj_str)
('x', {'long_name': b'x coordinate of projection'
       'standard_name': b'projection_x_coordinate'
       'units': b'm'})
>>>
>>> nc_proj.nc_ycoord_attrs(proj_str)
('y', {'long_name': b'y coordinate of projection'
       'standard_name': b'projection_y_coordinate'
       'units': b'm'})
>>>
>>> geo_transform = [-5563975.716997, 1998.657845, 0.,
>>>                  5564551.163068, 0., -1998.657845]
>>> x_size, y_size = (10, 5)
>>> nc_proj.nc_coord_values(geo_transform, x_size, y_size, False)
([-5562976.3880745005, -5560977.730229501, -5558979.0723845, -5556980.4145395, -5554981.7566945,
  -5552983.0988495005, -5550984.441004501, -5548985.7831595, -5546987.1253145, -5544988.4674695],
 [5563551.8341455, 5561553.1763005005, 5559554.5184555, 5557555.8606105, 5555557.2027655])

```
