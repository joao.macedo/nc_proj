Create anaconda virtual env gdal_env:
conda create -n gdal_env

Install gdal deps with:
conda install --only-deps -n gdal_env -c conda-forge gdal

If libspatialite is forcing proj4 old version installation:
conda install -n gdal_env -c conda-forge numpy geos proj4 netcdf4 libboost \
    json-c freexl giflib xerces-c libkml kealib libdap4 poppler libpq cairo \
    openjpeg glib geotiff postgresql tzcode
conda install -n gdal_env -c conda-forge --no-deps libspatialite

LDFLAGS="-L/<path>/<to>/<anaconda>/envs/gdal_env/lib -Wl,-rpath -Wl,/<path>/<to>/<anaconda>/envs/gdal_env/lib" \
CPPFLAGS="-I/<path>/<to>/<anaconda>/envs/gdal_env/include" \
./configure --prefix /<path>/<to>/<anaconda>/envs/gdal_env \
--with-libjson-c=/<path>/<to>/<anaconda>/envs/gdal_env \
--with-python=yes

make

make install
