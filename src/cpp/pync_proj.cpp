#include "pync_proj.h"

PyObject* nc_grid_mapping_attrs(const char *proj_str) {
    PyObject *ncgm_dict = PyDict_New();
    std::pair< std::map<std::string, std::string>,
               std::map<std::string, double> > ncgm_attrs;
    std::map<std::string, std::string>::iterator str_attr_it;
    std::map<std::string, double>::iterator float_attr_it;

    ncgm_attrs = get_grid_mapping_attrs(proj_str);

    for(str_attr_it = ncgm_attrs.first.begin();
        str_attr_it != ncgm_attrs.first.end(); ++str_attr_it) {
        PyDict_SetItemString(ncgm_dict, str_attr_it->first.c_str(),
                             PyBytes_FromString(str_attr_it->second.c_str()));
    }

    for(float_attr_it = ncgm_attrs.second.begin();
        float_attr_it != ncgm_attrs.second.end(); ++float_attr_it) {
        PyDict_SetItemString(ncgm_dict, float_attr_it->first.c_str(),
                             PyFloat_FromDouble(float_attr_it->second));
    }
    return ncgm_dict;
}


PyObject* nc_xcoord_attrs(const char *proj_str) {

    PyObject *x_coord = PyTuple_New(2);
    PyObject *xcoord_attrs = PyDict_New();
    std::pair< std::string,
               std::map<std::string, std::string> > coord_attrs;
    std::map<std::string, std::string>::iterator str_attr_it;

    /* Get x coordinate attributes: */
    coord_attrs = get_xcoord_attrs(proj_str);
    for(str_attr_it = coord_attrs.second.begin();
        str_attr_it != coord_attrs.second.end();
        ++str_attr_it) {

        PyDict_SetItemString(xcoord_attrs, str_attr_it->first.c_str(),
                             PyBytes_FromString(str_attr_it->second.c_str()));
    }
    PyTuple_SetItem(x_coord, 0, PyUnicode_FromString(coord_attrs.first.c_str()));
    PyTuple_SetItem(x_coord, 1, xcoord_attrs);
    return x_coord;
}


PyObject* nc_ycoord_attrs(const char *proj_str) {

    PyObject *y_coord = PyTuple_New(2);
    PyObject *ycoord_attrs = PyDict_New();
    std::pair< std::string,
               std::map<std::string, std::string> > coord_attrs;
    std::map<std::string, std::string>::iterator str_attr_it;

    /* Get y coordinate attributes: */
    coord_attrs = get_ycoord_attrs(proj_str);
    for(str_attr_it = coord_attrs.second.begin();
        str_attr_it != coord_attrs.second.end();
        ++str_attr_it) {

        PyDict_SetItemString(ycoord_attrs, str_attr_it->first.c_str(),
                             PyBytes_FromString(str_attr_it->second.c_str()));
    }
    PyTuple_SetItem(y_coord, 0, PyUnicode_FromString(coord_attrs.first.c_str()));
    PyTuple_SetItem(y_coord, 1, ycoord_attrs);
    return y_coord;
}


PyObject* nc_coord_values(PyObject *geo_transform, size_t xSize, size_t ySize, bool bottomUp) {
    double adfGeoTransform[6];
    std::pair< double*, double* > coords_pair;
    PyObject *xcoords = PyList_New(xSize),
             *ycoords = PyList_New(ySize);
    PyObject *coords_tuple = PyTuple_Pack(2, xcoords, ycoords);
    PyObject *geoTrfIter = PyObject_GetIter(geo_transform);

    for( size_t i = 0; i < 6; i++ ) {
        PyObject *obj = PyIter_Next(geoTrfIter);
        if (obj == NULL) {
            printf("geo_transform argument must have size 6!\n");
            PyErr_BadArgument();
            return coords_tuple;
        }
        adfGeoTransform[i] = PyFloat_AsDouble(obj);
    }
    coords_pair = get_coord_values(adfGeoTransform, xSize, ySize, bottomUp);
    
    for( size_t i = 0; i < xSize; i++ )
        PyList_SetItem(xcoords, i, PyFloat_FromDouble(coords_pair.first[i]));

    for( size_t j = 0; j < ySize; j++ )
        PyList_SetItem(ycoords, j, PyFloat_FromDouble(coords_pair.second[j]));

    CPLFree(coords_pair.first);
    CPLFree(coords_pair.second);

    return coords_tuple;
}
