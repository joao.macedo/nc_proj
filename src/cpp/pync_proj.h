#ifndef pync_proj
#define pync_proj
#include <Python.h>
#include "nc_proj.h"

PyObject* nc_grid_mapping_attrs(const char *proj_str);
PyObject* nc_xcoord_attrs(const char *proj_str);
PyObject* nc_ycoord_attrs(const char *proj_str);
PyObject* nc_coord_values(PyObject *geo_transform, size_t xSize, size_t ySize, bool bottomUp=false);

#endif
