
/*
MIT License

Copyright (c) 2018 Joao Macedo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "nc_proj.h"

std::map<std::string, std::string> NCGM_STR_ATTRS;
std::map<std::string, double> NCGM_FLOAT_ATTRS;


/* Override netcdf library functions: */

const char *nc_strerror(int ncerr) {
    return "Override nc_strerror!";
}


int nc_put_att_double(int ncid, int varid, const char *name,
                      nc_type xtype, size_t len, const double *op) {
    if (varid == -1)
        NCGM_FLOAT_ATTRS[name] = *op;
    return 0;
}


int nc_put_att_text(int ncid, int varid, const char *name,
                size_t len, const char *op) {
    if (varid == -1)
        NCGM_STR_ATTRS[name] = std::string(op, len);
    return 0;
}


int nc_def_var(int ncid, const char *name, nc_type xtype, int ndims,
           const int *dimidsp, int *varidp) {
    //printf("crs: %s; ndims: %d\n", name, ndims);
    return 0;
}


/* Get netcdf grid_mapping (string and double)
   attributes, from PROJ string:
*/
std::pair< std::map<std::string, std::string>,
           std::map<std::string, double> > get_grid_mapping_attrs(const char *proj_str) {
    int cdfid = -1;
    int NCDFVarID = -1;
    OGRSpatialReference oSRS;

    NCGM_STR_ATTRS.clear();
    NCGM_FLOAT_ATTRS.clear();

    oSRS.importFromProj4(proj_str);

    if(oSRS.IsProjected()) {
        char *pszCFProjection = nullptr;
        const char *pszProjName = oSRS.GetAttrValue("PROJECTION");

        for(int i = 0; poNetcdfSRS_PT[i].WKT_SRS != nullptr; i++) {

            if( EQUAL(poNetcdfSRS_PT[i].WKT_SRS, pszProjName) )
            {
                pszCFProjection = CPLStrdup(poNetcdfSRS_PT[i].CF_SRS);
                break;
            }
        }

        if( pszCFProjection == nullptr ) {
            return std::pair< std::map<std::string, std::string>,
                              std::map<std::string, double> >();
        } else {
            NCDFWriteSRSVariable(cdfid, &oSRS, &pszCFProjection, false);
            if( EQUAL(pszProjName, SRS_PT_GEOSTATIONARY_SATELLITE) ) {
                const char *pszPredefProj4 =
                    oSRS.GetExtension(oSRS.GetRoot()->GetValue(),
                                      "PROJ4", nullptr);
                const char *pszSweepAxisAngle = (pszPredefProj4 != nullptr and
                    strstr(pszPredefProj4, "+sweep=x"))? "x": "y";
                nc_put_att_text(cdfid, NCDFVarID, CF_PP_SWEEP_ANGLE_AXIS,
                                strlen(pszSweepAxisAngle), pszSweepAxisAngle);
            }
        }
    } else {
            nc_put_att_text(cdfid, NCDFVarID, CF_GRD_MAPPING_NAME,
                            strlen(CF_PT_LATITUDE_LONGITUDE), CF_PT_LATITUDE_LONGITUDE);
    }

    nc_put_att_text(cdfid, NCDFVarID, CF_LNG_NAME,
                    strlen("CRS definition"), "CRS definition");

    // Write CF-1.5 compliant common attributes.
    // DATUM information.
    double dfTemp = oSRS.GetPrimeMeridian();
    nc_put_att_double(cdfid, NCDFVarID, CF_PP_STD_PARALLEL,
                      NC_DOUBLE, 1, &dfTemp);
    dfTemp = oSRS.GetSemiMajor();
    nc_put_att_double(cdfid, NCDFVarID, CF_PP_SEMI_MAJOR_AXIS,
                      NC_DOUBLE, 1, &dfTemp);
    dfTemp = oSRS.GetInvFlattening();
    nc_put_att_double(cdfid, NCDFVarID, CF_PP_INVERSE_FLATTENING,
                      NC_DOUBLE, 1, &dfTemp);

    return std::pair< std::map<std::string, std::string>,
                      std::map<std::string, double> > (NCGM_STR_ATTRS,
                                                       NCGM_FLOAT_ATTRS);

}


std::pair< std::string,
           std::map<std::string, std::string> > get_xcoord_attrs(const char *proj_str) {
    int cdfid = -1;
    int nVarXID = -1,
        nVarYID = -2;
    std::string coord_name;
    OGRSpatialReference oSRS;

    NCGM_STR_ATTRS.clear();
    oSRS.importFromProj4(proj_str);

    if(oSRS.IsProjected()) {
        coord_name = std::string(CF_PROJ_X_VAR_NAME);
        NCDFWriteXYVarsAttributes(cdfid, nVarXID, nVarYID, &oSRS);
    } else {
        coord_name = std::string(CF_LONGITUDE_VAR_NAME);
        NCDFWriteLonLatVarsAttributes(cdfid, nVarXID, nVarYID);
    }

    return std::pair< std::string,
                      std::map<std::string, std::string> > (coord_name,
                                                            NCGM_STR_ATTRS);
}


std::pair< std::string,
           std::map<std::string, std::string> > get_ycoord_attrs(const char *proj_str) {
    int cdfid = -1;
    int nVarXID = -2,
        nVarYID = -1;
    std::string coord_name;
    OGRSpatialReference oSRS;

    NCGM_STR_ATTRS.clear();
    oSRS.importFromProj4(proj_str);

    if(oSRS.IsProjected()) {
        coord_name = std::string(CF_PROJ_Y_VAR_NAME);
        NCDFWriteXYVarsAttributes(cdfid, nVarXID, nVarYID, &oSRS);
    } else {
        coord_name = std::string(CF_LATITUDE_VAR_NAME);
        NCDFWriteLonLatVarsAttributes(cdfid, nVarXID, nVarYID);
    }

    return std::pair< std::string,
                      std::map<std::string, std::string> > (coord_name,
                                                            NCGM_STR_ATTRS);
}



std::pair< double*, double* > get_coord_values(double adfGeoTransform[6],
                                               size_t nRasterXSize,
                                               size_t nRasterYSize,
                                               bool bBottomUp) {
    // Get projection values.

    double dfX0 = 0.0;
    double dfDX = 0.0;
    double dfY0 = 0.0;
    double dfDY = 0.0;

    CPLDebug("GDAL_netCDF", "Getting (X,Y) values");

    double* padXVal =
        static_cast<double *>(CPLMalloc(nRasterXSize * sizeof(double)));
    double* padYVal =
        static_cast<double *>(CPLMalloc(nRasterYSize * sizeof(double)));

    // Get Y values.
    if( !bBottomUp )
        dfY0 = adfGeoTransform[3];
    else  // Invert latitude values.
        dfY0 = adfGeoTransform[3] + (adfGeoTransform[5] * nRasterYSize);
    dfDY = adfGeoTransform[5];

    for( size_t j = 0; j < nRasterYSize; j++ )
    {
        // The data point is centered inside the pixel.
        if( !bBottomUp )
            padYVal[j] = dfY0 + (j + 0.5) * dfDY;
        else  // Invert latitude values.
            padYVal[j] = dfY0 - (j + 0.5) * dfDY;
    }

    // Get X values.
    dfX0 = adfGeoTransform[0];
    dfDX = adfGeoTransform[1];

    for( size_t i = 0; i < nRasterXSize; i++ )
    {
        // The data point is centered inside the pixel.
        padXVal[i] = dfX0 + (i + 0.5) * dfDX;
    }

    return std::pair< double*, double* > (padXVal, padYVal);
}
