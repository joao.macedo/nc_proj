/* swig interface file: nc_proj.i */
  
%module nc_proj 
%{ 
    #define SWIG_FILE_WITH_INIT
    #include "pync_proj.h" 
%} 
 
 
%feature("autodoc", "0");
PyObject* nc_grid_mapping_attrs(const char *proj_str);

%feature("autodoc", "0");
PyObject* nc_xcoord_attrs(const char *proj_str);

%feature("autodoc", "0");
PyObject* nc_ycoord_attrs(const char *proj_str);

%feature("autodoc", "0");
PyObject* nc_coord_values(PyObject *geo_transform, size_t xSize,
                          size_t ySize, bool bottomUp=false);
