#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
setup.py file for SWIG example
From http://www.swig.org/Doc3.0/Python.html#Python_importfrominit
"""

from distutils.core import setup, Extension

nc_proj_module = Extension(
    'nc_proj._nc_proj',
    include_dirs = ['src/cpp',
                    'src/cpp/netcdf'],
    libraries = ['gdal'],
    sources = [#'src/python/nc_proj/nc_proj.i',
               'src/cpp/nc_proj_warp.cpp',
               'src/cpp/nc_proj.cpp',
               'src/cpp/pync_proj.cpp',
               'src/cpp/netcdf/gmtdataset.cpp',
               'src/cpp/netcdf/netcdfwriterconfig.cpp',
               'src/cpp/netcdf/netcdflayer.cpp',
               'src/cpp/netcdf/netcdfdataset.cpp'],
    #swig_opts=['-c++',],
    extra_compile_args = ['-std=c++11'],
    language = 'c++11',
)

setup (
    name = 'nc_proj',
    version = '1.0.0',
    author = "João Macedo",
    description = "Create NetCDF Projection variables/attributes from PROJ string.",
    ext_modules = [nc_proj_module],
    packages=['nc_proj'],
    package_dir={'nc_proj': 'src/python/nc_proj'},
)
