#include "nc_proj.h"

#define NCDF_DEBUG 1

int main()
{

    /* NetCDF grid_mapping (string and double) attributes */
    std::pair< std::map<std::string, std::string>,
               std::map<std::string, double> > ncgm_attrs;

    /* NetCDF coordinate name and (string) attributes */
    std::pair< std::string,
               std::map<std::string, std::string> > coord_attrs;

    std::map<std::string, std::string>::iterator str_attr_it;
    std::map<std::string, double>::iterator float_attr_it;

    const char *proj_str;

    proj_str = "+proj=geos +lon_0=0 +h=35785831 +x_0=0 +y_0=0 +a=6378137 b=6356752.3 +units=m +sweep=y +no_defs"

    printf("\nPROJ string:\n\t%s\n\n", proj_str);

    printf("CRS (Coordinates Reference System) attributes:\n");

    ncgm_attrs = get_grid_mapping_attrs(proj_str);

    for(str_attr_it = ncgm_attrs.first.begin();
        str_attr_it != ncgm_attrs.first.end();
        ++str_attr_it) {
        printf("\t%s: %s\n", str_attr_it->first.c_str(),
                             str_attr_it->second.c_str());
    }

    for(float_attr_it = ncgm_attrs.second.begin();
        float_attr_it != ncgm_attrs.second.end();
        ++float_attr_it) {
        printf("\t%s: %f\n", float_attr_it->first.c_str(),
                             float_attr_it->second);
    }

    coord_attrs = get_xcoord_attrs(proj_str);

    printf("\n%s coordinate attributes:\n",
           coord_attrs.first.c_str());

    for(str_attr_it = coord_attrs.second.begin();
        str_attr_it != coord_attrs.second.end();
        ++str_attr_it) {
        printf("\t%s: %s\n", str_attr_it->first.c_str(),
                             str_attr_it->second.c_str());
    }

    coord_attrs = get_ycoord_attrs(proj_str);

    printf("\n%s coordinate attributes:\n",
           coord_attrs.first.c_str());

    for(str_attr_it = coord_attrs.second.begin();
        str_attr_it != coord_attrs.second.end();
        ++str_attr_it) {
        printf("\t%s: %s\n", str_attr_it->first.c_str(),
                             str_attr_it->second.c_str());
    }


    double adfGeoTransform[6] = {-5563975.716997, 1998.657845, 0,
                                 5564551.163068, 0, -1998.657845};

    size_t xSize=10,
           ySize=10;

    std::pair< double*, double* > coord_values = get_coord_values(adfGeoTransform,
                                                                  xSize, ySize); 


    printf("\nx values:\n");
    for( size_t i = 0; i < xSize; i++ )
    {
        // The data point is centered inside the pixel.
        printf("%f ", *(coord_values.first + i));
    }

    printf("\ny values:\n");
    for( size_t j = 0; j < ySize; j++ ) {
        // The data point is centered inside the pixel.
        printf("%f ", *(coord_values.second + j));
    }


    /* Test Geodetic coordinates: */


    proj_str = "+proj=longlat +datum=WGS84 +ellps=WGS84";

    printf("\nPROJ string:\n\t%s\n\n", proj_str);

    printf("CRS (Coordinates Reference System) attributes:\n");

    ncgm_attrs = get_grid_mapping_attrs(proj_str);

    for(str_attr_it = ncgm_attrs.first.begin();
        str_attr_it != ncgm_attrs.first.end();
        ++str_attr_it) {
        printf("\t%s: %s\n", str_attr_it->first.c_str(),
                             str_attr_it->second.c_str());
    }

    for(float_attr_it = ncgm_attrs.second.begin();
        float_attr_it != ncgm_attrs.second.end();
        ++float_attr_it) {
        printf("\t%s: %f\n", float_attr_it->first.c_str(),
                             float_attr_it->second);
    }

    coord_attrs = get_xcoord_attrs(proj_str);

    printf("\n%s coordinate attributes:\n",
           coord_attrs.first.c_str());

    for(str_attr_it = coord_attrs.second.begin();
        str_attr_it != coord_attrs.second.end();
        ++str_attr_it) {
        printf("\t%s: %s\n", str_attr_it->first.c_str(),
                             str_attr_it->second.c_str());
    }

    coord_attrs = get_ycoord_attrs(proj_str);

    printf("\n%s coordinate attributes:\n",
           coord_attrs.first.c_str());

    for(str_attr_it = coord_attrs.second.begin();
        str_attr_it != coord_attrs.second.end();
        ++str_attr_it) {
        printf("\t%s: %s\n", str_attr_it->first.c_str(),
                             str_attr_it->second.c_str());
    }

}

