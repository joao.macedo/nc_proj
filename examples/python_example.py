#!/usr/bin/env python
#-*- coding:utf-8 -*-

from nc_proj import nc_proj

def print_ncproj_attributes():
    proj_str = ("+proj=geos +lon_0=0 +h=35785831 "
                "+x_0=0 +y_0=0 +a=6378137 b=6356752.3 "
                "+units=m +sweep=y +no_defs")

    ncgm_dict = nc_proj.nc_grid_mapping_attrs(proj_str)

    print("\nPROJ string:\n\t{}\n\n"
          "CRS (Coordinates Reference System) attributes:".format(proj_str))

    for name, value in ncgm_dict.items():
        print("\t{}: {}".format(name, value))

    xcoord_name, attrs_dict = nc_proj.nc_xcoord_attrs(proj_str)
    print("\n{} coordinate attributes:".format(xcoord_name))
    for name, value in attrs_dict.items():
        print("\t{}: {}".format(name, value))

    ycoord_name, attrs_dict = nc_proj.nc_ycoord_attrs(proj_str)
    print("\n{} coordinate attributes:".format(ycoord_name))
    for name, value in attrs_dict.items():
        print("\t{}: {}".format(name, value))

    geo_transform = (-5570253.667650278, 3000.4059615676147, 0.0,
                     5570253.667650278, 0.0, -3000.4059615676147)

    x_coords, y_coords = nc_proj.nc_coord_values(
        geo_transform, 10, 10
    )

    print("\n{} values:\n\t{}\n\n{} values:\n\t{}\n"
          "".format(xcoord_name, x_coords, ycoord_name, y_coords))


def write_netCDF4(output_path, var_name, data, proj_str, geo_transform,
                  fill_value=None):
    """Example of a function that creates a Georeferenced netCDF file,
    using the netCDF4-python module interface, from a given numpy ndarray
    2D with its projection and horizontal boundaries/extent defined by
    the 'proj_str'and 'geo_transform' arguments respectively.
    """
    from netCDF4 import Dataset

    y_size, x_size = data.shape

    # Get crs (Coordinates Reference System) variable attributes
    gm_attrs = nc_proj.nc_grid_mapping_attrs(proj_str)

    # Get x/y variable name and attributes:
    x_coord_name, x_attrs = nc_proj.nc_xcoord_attrs(proj_str)
    y_coord_name, y_attrs = nc_proj.nc_ycoord_attrs(proj_str)

    # Get x/y coordinates values:
    x, y = nc_proj.nc_coord_values(geo_transform, x_size, y_size)




    # Create output netCDF file:
    dset = Dataset(output_path, 'w')

    # Create crs variable:
    grid_mapping_name = gm_attrs['grid_mapping_name'].decode()
    crs = dset.createVariable(grid_mapping_name, 'S1')
    # Create crs attributes:
    for attr_name, attr_value in gm_attrs.items():
       crs.setncattr(attr_name, attr_value)

    # Create y dim:
    dim_y = dset.createDimension(y_coord_name, y_size)
    nc_y = dset.createVariable(y_coord_name, 'f8', (y_coord_name,))
    nc_y[:] = y
    # Create y dim attributes:
    for attr_name, attr_value in y_attrs.items():
       nc_y.setncattr(attr_name, attr_value)

    # Create x dim:
    dim_x = dset.createDimension(x_coord_name, x_size)
    nc_x = dset.createVariable(x_coord_name, 'f8', (x_coord_name,))
    nc_x[:] = x
    # Create x dim attributes:
    for attr_name, attr_value in x_attrs.items():
       nc_x.setncattr(attr_name, attr_value)

    # Create variable:
    nc_var = dset.createVariable(
        var_name, data.dtype, (y_coord_name, x_coord_name),
        fill_value=fill_value
    )
    nc_var[:,:] = data
    # Link variable with the crs variable:
    nc_var.setncattr('grid_mapping', gm_attrs['grid_mapping_name'])

    dset.close() 


if __name__ == "__main__":
    print_ncproj_attributes()
